import React, { Component } from "react";
import { Route, Switch, Redirect} from "react-router-dom";
import { Login } from "./components/Login";
import { Salon } from "./components/Salon";
import { Projector } from "./components/Projector";
import io from 'socket.io-client';

const __URL = window.location.host; // http://.....
const Image = () => {
  return <div id="quiz-image"><img src="https://media1.giphy.com/media/nUfZ8ne8voiLC/source.gif" alt="quiz" /></div>;
}
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      room: "",
      redirect: false,
      socket: io('http://localhost:1337')
    };
  };

  componentDidMount() {
    const { socket } = this.state;
    socket.on('projector_room', response => {
      this.setState({
        room: response,
        redirect: true
      });
    });
  }

  renderRedirect() {
    if (this.state.redirect) {
      return <Redirect to={'/projector/' + this.state.room} ></Redirect>;
    }
  }
  render() {
    return (
        <Switch>
          <Route exact path="/" component={Image} />
          <Route exact path="/projector" component={Image} />
          <Route exact path="/projector/:id" component={Projector} />
          <Route exact path="/:id" component={Image} />
          <Route exact path="/:id/login" component={Login} />
          <Route exact path="/:id/quiz" component={Salon} />
        </Switch>
    );
  }
}
export default App;
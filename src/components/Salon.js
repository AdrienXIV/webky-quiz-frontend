import React from 'react';
import io from 'socket.io-client';
import ReactHtmlParser from 'react-html-parser';
import { WaitingAdmin } from './WaitingAdmin';

export class Salon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pseudo: sessionStorage.getItem('user') === null ? '' : sessionStorage.getItem('user'),
            game_connected: sessionStorage.getItem('game_connected') === null ? false : sessionStorage.getItem('game_connected'),
            text: "",
            room: this.props.match.params.id,
            numbers_players: 0,
            redirect: false,
            connected: false,
            socket: io('http://localhost:1337')
        };
    };


    UNSAFE_componentWillMount() {
        const { socket, room, pseudo } = this.state;
        console.log(pseudo)
        if (pseudo === '') {
            this.setState({
                redirect: true,
                path: 'login'
            });
        } else {
            socket.emit('login', {
                room,
                pseudo
            });
        }
        this.setState({
            text: ""
        });

        /*if (!connected) {
            socket.emit("login", {
                room,
                pseudo
            }).addEventListener('user_session', response => {
                
                sessionStorage.setItem('socket_id', response);
            });
        }*/
        socket.emit('game_info', room).addEventListener('players', response => {

            console.log(response)
            let text = "";
            response.players.forEach(elem => {
                text += "<p>" + elem.pseudo + "</p>";
            });
            this.setState({
                numbers_players: response.number,
                game_connected: true,
                text: text,
                connected: true
            });
        });

        socket.on('start', response => {
            // être lié à la partie 
            sessionStorage.setItem('game_connected', true);

            console.log(response)
            if (response === 1) {
                this.setState({
                    redirect: true
                });
            }
        });
    }

    UNSAFE_componentWillUpdate() {
        const { socket } = this.state;
        setInterval(() => {
            if (socket.disconnected) {
                console.log('disconnected');
                this.setState({
                    connected: false
                });
            } else if (socket.connected) {
                console.log('connected')
            }
        }, 1000);
    }

    render() {
        if (this.state.redirect) {
            return <WaitingAdmin room={this.state.room} socket={this.state.socket} numbers_players={this.state.numbers_players} />
        } else if (this.state.game_connected) {
            return <WaitingAdmin room={this.state.room} socket={this.state.socket} numbers_players={this.state.numbers_players} />
        }
        else {
            return <div id="salon">
                <h5 className="ui header">Joueurs connectés</h5>
                <h2 className="ui center aligned icon header">
                    <i className="circular users icon"></i>
                    {this.state.numbers_players}
                </h2>
                <div id="players-connected" className="ui raised very padded text container segment">
                    {ReactHtmlParser(this.state.text)}
                </div>
                <div id="loader">
                    <div className="ui active inline loader"></div>
                    <small>En attente de l'administrateur.</small>
                </div>
            </div>
        }
    }
}
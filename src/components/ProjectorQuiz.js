import React from 'react';
import io from 'socket.io-client';
import { Progress } from 'semantic-ui-react'
import $ from 'jquery';

export class ProjectorQuiz extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            seconde: 30,
            time: 30, // temps de référence
            pseudo: "",
            text: "",
            room: this.props.room,
            numbers_players: 0,
            response: false,
            redirect: false,
            socket: io('http://localhost:1337')
        };
        this.chrono_interval = setInterval(() => { }, 100);

    };

    componentDidMount() {
        const { socket, room } = this.state;
        socket.emit('projector', room);
    }

    UNSAFE_componentWillMount() {
        const { socket } = this.state;
        this.setState({
            text: ""
        });
        socket.on('players', response => {
            console.log(response)
            let text = "";
            response.players.forEach(elem => {
                text += "<p>" + elem.pseudo + "</p>";
            });
            this.setState({
                numbers_players: response.number,
                text: text
            });
        });

        socket.on('show_question', response => {
            this.setState({
                seconde: response.time / 1000,
                time: response.time / 1000
            });

            $('#quiz-image').hide(); // enlever l'image du début

            $('#chrono').css('display', 'flex !important').fadeIn(2000);
            $('#question').html(''); // remise à 0
            $('#replies').html(''); // remise à 0

            let question = response.question;
            let content = '<div className="column" data-id="' + question.id + '" data-order="' + question.order + '">' + question.name + '</div>';
            $('#question').html(content);
        });

        socket.on('show_reply', response => {
            let reply = response.reply;
            let content = '<div className="column" id="' + reply.id + '">' + reply.name + '</div>';
            $('#replies').append(content);

        });

        socket.on('show_good_reply', response => {
            $('#' + response).css({
                'background-color': '#F79F1F',
                'color': 'white'
            });
        });

        socket.on('end_quiz', response => {
            if (response === 1) {
                $('#chrono').hide();
                $('#question').html('');
                $('#replies').html('');
                $('#end_quiz').fadeIn(2000)

                //TODO: afficher classement final
            }
        });

        socket.on('start_chrono', time => {
            $('#valider').show();
            this.chrono(time);
        });
        socket.on('end_chrono', time => {
            this.end_chrono();
        });

        socket.on('show_classement', response => {
            response.sort(function (a, b) {
                return b.score - a.score;
            });
            console.log(response)
            this.show_classement(response)
        });

    }

    chrono(time) {
        $('#valider button').removeClass('disabled');
        $('#valider').show();

        this.setState({
            seconde: (time / 1000)
        });

        let value = this.state.seconde;
        this.chrono_interval = setInterval(() => {
            this.setState({
                seconde: (value - 0.1).toFixed(2)
            });
            console.log(this.state.seconde)
            if (Number(this.state.seconde) === 0) {
                clearInterval(this.chrono_interval);
                $('#valider button').addClass('disabled');
                $('#valider').hide();
            }
            else {
                value = this.state.seconde;
            }
        }, 100);
    }

    show_classement(response) {
        let content = '';
        let i = 1;
        response.forEach(elem => {
            content += '<tr>';
            content += '<td><i className="angle right icon" style={{"color:#f39c12"}}> ' + 0 + '</i></td>' // position +0

            content += '<td>' + elem.player + '</td>';
            content += '<td>' + elem.score + '</td>';
            content += '<td>' + i + '</td>';
            content += '</tr>';
            i++;
        });
        $('#classement').html(content);
    }

    end_chrono() {
        $('#valider button').addClass('disabled');
        $('#valider').hide();
        
        this.setState({
            seconde: 0 // si y'a décallage
        });
        clearInterval(this.chrono_interval);
    }

    render() {

        return (
            <div className="ui fluid container" style={{ height: 'inherit', position: 'absolute' }}>
                <div id="chrono">
                    <Progress percent={this.state.seconde * (100 / this.state.time)} indicating label={this.state.seconde + " s"} size="big" />
                </div>
                <div id="quiz-image"><img src="https://i.pinimg.com/originals/ca/52/93/ca52931adbf2a45198ed84d1ebb01c55.png" alt="quiz" /></div>
                <div className="ui two column centered grid" id="question">

                </div>
                <div className="ui stackable four column grid" id="replies">

                </div>

                <div id="end_quiz">Fin du QUIZ !</div>
                <div className="ui height column centered grid quiz-question-reponses"></div>
            </div>
        );
    }
}
import React from 'react';

export class Reply extends React.Component {
    constructor(props) {
        super();
    }

    componentWillReceiveProps(newProps){
    }

    render() {
        return <div className="column ui button" onClick={this.props.onClick} id={this.props.id}>{this.props.name}</div>;
    }
} 

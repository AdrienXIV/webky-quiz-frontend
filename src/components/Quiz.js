import React from 'react';
import { Progress } from 'semantic-ui-react'
import { Redirect } from 'react-router-dom';
import $ from 'jquery';
import { Reply } from './Reply';
import { WaitingAdmin } from './WaitingAdmin';


export class Quiz extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            seconde: this.props.time, // temps à décrémenter
            time: this.props.time, // temps de référence
            pseudo: sessionStorage.getItem('user') === null ? '' : sessionStorage.getItem('user'),
            text: "",
            waiting: false,
            idValidate: false,
            user_reply: -99999, // fausse réponse
            room: this.props.room,
            numbers_players: this.props.number_players,
            redirect: false,
            socket: this.props.socket
        };
        this.chrono_interval = setInterval(() => { }, 100);

        this.handleClick = this.handleClick.bind(this);
        this.handleClickResponse = this.handleClickResponse.bind(this);
    };

    componentDidMount() {
        const { socket, room } = this.state;

        socket.emit('game_info', room);

        $('#quiz-image').hide(); // enlever l'image du début
        $('#chrono').show();

        // lancement du chrono
        this.chrono(this.props.time);

    }
    componentDidUpdate() {
        const { socket } = this.state;

        socket.on('users_waiting', (res) => {
            if (res === 1) {
                this.setState({
                    waiting: true
                });
            }
        });

        socket.on('show_good_reply', response => {
            $('#replies div').removeClass('disabled');
            $('#replies div').removeClass('user_reply');

            if (Number(response) === Number(this.state.user_reply)) { // bonne réponse
                $('#' + response).addClass('good_reply');
            } else {// mauvaise réponse
                $('#' + response).addClass('good_reply'); // affichage de la bonne réponse en vert
                $('#' + String(this.state.user_reply)).addClass('bad_reply'); // affichage de la réponse du joueur en rouge
            }
        });

        socket.on('end_quiz', response => {
            if (response === 1) {
                $('#chrono').hide();
                $('#valider').hide();
                $('#question').hide();
                $('#replies').hide();
                $('#end_quiz').fadeIn(1000).delay(3000).hide()

                $('#quiz-image').delay(4000).fadeIn(1000);

                setTimeout(() => {
                    socket.emit('disconnect', this.state.room);

                    // destruction de la session
                    sessionStorage.clear('user');
                    sessionStorage.clear('game_connected');

                    this.setState({
                        redirect: true,
                    });
                }, 10000);
            }
        });

        socket.on('end_chrono', () => {
            this.end_chrono();
        });

    }

    chrono(time) {
        $('#valider button').removeClass('disabled');
        $('#valider').show();

        this.setState({
            seconde: time,
            time
        });

        let value = this.state.seconde;
        this.chrono_interval = setInterval(() => {
            this.setState({
                seconde: (value - 0.1).toFixed(2)
            });

            if (Number(this.state.seconde) === 0) {
                clearInterval(this.chrono_interval);
                $('#valider button').addClass('disabled');
                $('#valider').hide();
            }
            else {
                value = this.state.seconde;
            }
        }, 100);
    }

    end_chrono() {
        $('#valider button').addClass('disabled');
        $('#replies div').addClass('disabled');
        $('#valider').hide();

        clearInterval(this.chrono_interval);

        this.setState({
            seconde: 0 // si y'a décallage
        });

        const { socket, room, pseudo } = this.state;

        // si le joueur n'a pas validé, la valeur est envoyé à la fin du chrono
        if (!this.state.isValidate) {
            socket.emit('user_reply', {
                room,
                user_reply: this.state.user_reply,
                pseudo,
                question_id: this.props.question.id,
                question_number: this.props.question_number
            });
        }
    }

    handleClick(e) {
        $('#replies div').removeClass('user_reply');
        $('#' + e.target.id).addClass('user_reply');

        this.setState({
            user_reply: Number(e.target.id)
        });
    }

    handleClickResponse = () => {
        const { socket, room, pseudo } = this.state;

        $('#replies div').addClass('disabled');
        $('#valider button').addClass('disabled');
        $('#replies div').addClass('disabled');

        socket.emit('user_reply', {
            room,
            user_reply: this.state.user_reply,
            pseudo,
            question_id: this.props.question.id,
            question_number: this.props.question_number
        });

        this.setState({
            isValidate: true
        });
    }


    render() {
        if (this.state.redirect) {
            return <Redirect to={'/'} />
        }
        else if (this.state.waiting) {
            return <WaitingAdmin room={this.state.room} socket={this.state.socket} numbers_players={this.state.numbers_players} />
        }
        else {
            return (
                <div className="ui fluid container">
                    <div id="chrono">
                        <Progress percent={this.state.seconde * (100 / this.state.time)} indicating label={this.state.seconde + " s"} size="large" />
                    </div>
                    <div id="quiz-image"><img src="https://media1.giphy.com/media/nUfZ8ne8voiLC/source.gif" alt="quiz" /></div>
                    <div className="ui two column centered grid" id="question">
                        <div className="column" data-id={this.props.question.id} data-order={this.props.question.order}>{this.props.question.name}</div>
                        <div className="ui stackable four column grid" id="replies">
                            <Reply id={this.props.replies[0].id} name={this.props.replies[0].name} onClick={this.handleClick} />
                            <Reply id={this.props.replies[1].id} name={this.props.replies[1].name} onClick={this.handleClick} />
                            <Reply id={this.props.replies[2].id} name={this.props.replies[2].name} onClick={this.handleClick} />
                            <Reply id={this.props.replies[3].id} name={this.props.replies[3].name} onClick={this.handleClick} />
                        </div>
                    </div>
                    <div id="valider" style={{ display: 'none' }}>
                        <button className="ui inverted green button" onClick={this.handleClickResponse}>
                            Valider
            </button>
                    </div>

                    <div id="end_quiz">Fin du QUIZ !</div>
                    <div className="ui height column centered grid quiz-question-reponses"></div>
                </div>
            );
        }
    }
}
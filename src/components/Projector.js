import React from 'react';
import io from 'socket.io-client';
import ReactHtmlParser from 'react-html-parser';
import { ProjectorQuiz } from './ProjectorQuiz';
import $ from 'jquery';

/**
 * url -> url de la page sur le tel du joueur
 */
export class Projector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pseudo: "",
            text: "",
            url: "http://localhost:3000/" + this.props.match.params.id + "/login",
            room: this.props.match.params.id,
            numbers_players: 0,
            response: false,
            socket: io('http://localhost:1337')
        };
    };

    componentDidMount() {
        console.log(this.props)
        const { socket, room } = this.state;

        socket.emit('projector', room);
        socket.on('players', response => {
            console.log(response)
            let text = "";
            response.players.forEach(elem => {
                text += "<p>" + elem.pseudo + "</p>";
            });
            this.setState({
                numbers_players: response.number,
                text: text
            });
        });

        $('#qrcode').show();
    }

    UNSAFE_componentWillMount() {
        $('#quiz-image').hide();

        const { socket } = this.state;
        this.setState({
            text: ""
        });
        socket.on('players', response => {
            console.log(response)
            let text = "";
            response.players.forEach(elem => {
                text += "<p>" + elem.pseudo + "</p>";
            });
            this.setState({
                numbers_players: response.number,
                text: text
            });
        });
        socket.on('start', response => {
            $('#qrcode').hide();
            $('#wait-admin').hide();

            $('#show-classement').show();
            $('#quiz-image').fadeIn(1000);
        });

    }


    render() {
        return <div id="videoProj" className="ui two column grid">
            <div className="column" id='first-column'>
                <div className="ui segment" style={{ alignContent: 'center', display: 'none', margin: 0 }} id="qrcode">
                    <img src={__dirname + "qrcode/" + this.props.match.params.id + ".svg"} id="image-qrcode" alt="" />
                    <div>{this.state.url}</div>
                </div>
                <div className="ui segment" style={{ padding: 0 }}>
                    <ProjectorQuiz room={this.state.room} />
                </div>
            </div>
            <div className="column" id="wait-admin">
                <div className="ui segment">
                    <div id="salon">
                        <h5 className="ui header">Joueurs connectés</h5>
                        <h2 className="ui center aligned icon header">
                            <i className="circular users icon"></i>
                            {this.state.numbers_players}
                        </h2>
                        <div id="players-connected" className="ui raised very padded text ">
                            {ReactHtmlParser(this.state.text)}
                        </div>
                        <div id="loader">
                            <div className="ui active inline loader"></div>
                            <small>En attente de l'administrateur.</small>
                        </div>
                    </div>
                </div>
            </div>
            <div className="column" id="show-classement" style={{ display: 'none', 'alignSelf': 'center' }}>
                <div className="container" style={{ "marginTop": "1rem", "padding": "1rem" }}>
                    <table className="ui celled striped table">
                        <thead>
                            <tr>
                                <th width='5%'></th>
                                <th>Pseudo</th>
                                <th>Points</th>
                                <th width='10%'>Position</th>
                            </tr>
                        </thead>
                        <tbody id="classement">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    }
}
import React from 'react';
import { Quiz } from './Quiz';

export class WaitingAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pseudo: sessionStorage.getItem('user') === null ? '' : sessionStorage.getItem('user'),
            text: "",
            room: this.props.room,
            numbers_players: this.props.numbers_players,
            replies: [],
            question: {},
            question_number: 0,
            question_id: Number,
            redirect: false,
            connected: false,
            socket: this.props.socket
        };
    };

    UNSAFE_componentWillMount() {
        const { socket, room } = this.state;
        socket.emit('game_info', room);

        socket.on('user_start_answer', response => {
            this.setState({
                question: response.question,
                replies: response.replies,
                question_number: response.question_number,
                seconde: (response.time / 1000),
                time: (response.time / 1000),
                redirect: true
            });

            //sessionStorage.setItem('')

        });
    }

    render() {
        if (this.state.redirect) {
            return <Quiz time={this.state.time} room={this.state.room} socket={this.state.socket} numbers_players={this.state.numbers_players} replies={this.state.replies}
                question={this.state.question} question_number={this.state.question_number} />
        } else {
            return <div style={{ height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <div id="loader" style={{ fontSize: 'x-large', textAlign: 'center' }}>
                    <div className="ui active inline loader" style={{ marginBottom: '1vh' }}></div>
                    <small style={{ display: 'block' }}>En attente de l'administrateur.</small>
                </div>
            </div>
        }
    }
}
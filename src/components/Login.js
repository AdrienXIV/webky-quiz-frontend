import React from 'react';
import { Redirect } from 'react-router-dom'
import io from 'socket.io-client';

export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pseudo: "",
            error: "",
            room: this.props.match.params.id,
            redirect: false,
            socket: io('http://localhost:1337')
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    };

    UNSAFE_componentWillMount() {
        // session vierge
        sessionStorage.clear('user');
        sessionStorage.clear('game_connected');
    }

    handleChange = (e) => {
        this.setState({
            pseudo: e.target.value,
            error: ""
        });
    }

    handleClick = (e) => {
        const { pseudo } = this.state;

        sessionStorage.setItem('user', pseudo);

        this.setState({
            redirect: true
        });
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={'/' + this.state.room + '/quiz'} />
        } else {
            return (
                <div id="login">
                    <div className="ui left icon input">
                        <input type="text" value={this.state.pseudo} onChange={this.handleChange} placeholder="Pseudo" />
                        <i className="users icon"></i>
                        <button type="button" className="ui green basic button" onClick={this.handleClick}>Valider</button>
                    </div>
                    <div id="error">{this.state.error}</div>
                </div>
            );
        }
    }
}
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App'
import './css/style.css';

const __URL = window.location.host; // http://.....
ReactDOM.render(<BrowserRouter basename="/"><App/></BrowserRouter>, document.getElementById('root'));
